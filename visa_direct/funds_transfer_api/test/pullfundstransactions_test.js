/* ----------------------------------------------------------------------------------------------------------------------
* © Copyright 2018 Visa. All Rights Reserved.
*
* NOTICE: The software and accompanying information and documentation (together, the “Software”) remain the property of
* and are proprietary to Visa and its suppliers and affiliates. The Software remains protected by intellectual property
* rights and may be covered by U.S. and foreign patents or patent applications. The Software is licensed and not sold.
*
* By accessing the Software you are agreeing to Visa's terms of use (developer.visa.com/terms) and privacy policy
* (developer.visa.com/privacy). In addition, all permissible uses of the Software must be in support of Visa products,
* programs and services provided through the Visa Developer Program (VDP) platform only (developer.visa.com).
* THE SOFTWARE AND ANY ASSOCIATED INFORMATION OR DOCUMENTATION IS PROVIDED ON AN “AS IS,” “AS AVAILABLE,” “WITH ALL
* FAULTS” BASIS WITHOUT WARRANTY OR CONDITION OF ANY KIND. YOUR USE IS AT YOUR OWN RISK.
---------------------------------------------------------------------------------------------------------------------- */

'use strict';
var api = require('../src/ft_100').ft_100;
var authCredentials = require('../../../creds.json');
var express = require('express');
var app = express();
app.get('/', function (req, res) {
    res.send('Sending Pull!  '+ req.query.accounNumber);
    console.log("accounNumber  :"+req.query.accounNumber);
    var ft_100 = new api(authCredentials);

    // path invoked is '/visadirect/fundstransfer/v1/pullfundstransactions/{statusIdentifier}';
    ft_100.pullfunds(getParameters())
        .then(function(result) {
            // Put your custom logic here
            console.log('--------------- pullfundstransactions ---------------');
            console.log('Result is:' + JSON.stringify(result.body));
            console.log('\n');
        })
        .catch(function(error) {
            console.log('--------------- pullfundstransactions ---------------');
            console.log('Error is: ' + JSON.stringify(error));
            console.log('\n');
        });

    function getParameters() {
        var parameters = {

            "Accept": "application/json,application/octet-stream",
            "Content-Type": "application/json"
            //"Authorization":"Basic MkRYMllHMVI1WkQ3VEJVSkcxVUgyMTFYTDduejZmaFNMcnhRYjA2UkkxTU9XVVRMUTpBUTdLOEp4NDExbEVG"
        };
        parameters.payload = {
            "acquirerCountryCode": "841",
            "acquiringBin": "408999",
            "amount": "124.02",
            "businessApplicationId": "AA",
            "cardAcceptor": {
                "address": {
                    "country": "USA",
                    "county": "081",
                    "state": "CA",
                    "zipCode": "94404"
                },
                "idCode": "ABCD1234ABCD123",
                "name": "Visa Inc. USA-Foster City",
                "terminalId": "ABCD1234"
            },
            "cavv": "0700100038238906000013405823891061668252",
            "foreignExchangeFeeTransaction": "11.99",
            "localTransactionDateTime": "2018-10-20T22:39:03",
            "retrievalReferenceNumber": "330000550000",
            "senderCardExpiryDate": "2015-10",
            "senderCurrencyCode": "USD",
            "senderPrimaryAccountNumber": req.query.accounNumber,
            "surcharge": "11.99",
            "systemsTraceAuditNumber": "451001",
            "nationalReimbursementFee": "11.22",
            "cpsAuthorizationCharacteristicsIndicator": "Y",
            "addressVerificationData": {
            "street": "XYZ St",
            "postalCode": "12345"
            }
            };

        return parameters;
    }
});
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

