/* ----------------------------------------------------------------------------------------------------------------------
* © Copyright 2018 Visa. All Rights Reserved.
*
* NOTICE: The software and accompanying information and documentation (together, the “Software”) remain the property of
* and are proprietary to Visa and its suppliers and affiliates. The Software remains protected by intellectual property
* rights and may be covered by U.S. and foreign patents or patent applications. The Software is licensed and not sold.
*
* By accessing the Software you are agreeing to Visa's terms of use (developer.visa.com/terms) and privacy policy
* (developer.visa.com/privacy). In addition, all permissible uses of the Software must be in support of Visa products,
* programs and services provided through the Visa Developer Program (VDP) platform only (developer.visa.com).
* THE SOFTWARE AND ANY ASSOCIATED INFORMATION OR DOCUMENTATION IS PROVIDED ON AN “AS IS,” “AS AVAILABLE,” “WITH ALL
* FAULTS” BASIS WITHOUT WARRANTY OR CONDITION OF ANY KIND. YOUR USE IS AT YOUR OWN RISK.
---------------------------------------------------------------------------------------------------------------------- */

'use strict';
var api = require('../src/ft_100').ft_100;
var authCredentials = require('../../../creds.json');

var ft_100 = new api(authCredentials);

// path invoked is '/visadirect/fundstransfer/v1/reversefundstransactions/{statusIdentifier}';
ft_100.reversefundstransactions(getParameters())
    .then(function(result) {
        // Put your custom logic here
        console.log('--------------- reversefundstransactions ---------------');
        console.log('Result is:' + JSON.stringify(result.body));
        console.log('\n');
    })
    .catch(function(error) {
        console.log('--------------- reversefundstransactions ---------------');
        console.log('Error is: ' + JSON.stringify(error));
        console.log('\n');
    });

function getParameters() {
    var parameters = {

        "Accept": "application/json",
        "Content-Type": "application/json"
    };
    parameters.payload = {};

    return parameters;
}